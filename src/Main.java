import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");


//        Урок 7. Посимвольная обработка строк
//        Цель задания:
//        Формирование навыков работы с таким типом данных, как строки
//        Задание:
//        1.
//        Пользователь вводит пять строк. Выведите слово, состоящие из первой
//        буквы каждый строки

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner1 = new Scanner(System.in);
        String str1[] = new String[5];
        for (int i=0;i<5;i++){
            System.out.println("введите слово "+(i+1)+": ");
            str1[i] = scanner1.nextLine();
        }

        System.out.println("Получается такое слово из первых букв введённых слов:");
        for (int i=0;i<5;i++){
            System.out.printf(String.valueOf(str1[i].charAt(0)));
        }
        System.out.println("");

//        2.
//        Посчитайте количество нулей в числе
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner2 = new Scanner(System.in);
        String str2 = "";

        System.out.println("введите число : ");
        str2 = scanner2.nextLine();
        byte z2=0;
        for (int i=0;i<str2.length();i++){
           if (str2.charAt(i)=='0') z2++;
        }

        System.out.println("в этом числе всего столько нулей = "+z2);

//        3.
//        Найдите первую гласную букву в слове
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner3 = new Scanner(System.in);
        String str3 = "";

        System.out.println("введите слово : ");
        str3 = scanner3.nextLine();
        byte g3=0;
        char glasn[] = new char[]{'й','у','е','ъ','ы','а','о','э','я','и','ь','ю'};
        for (int i=0;i<str3.length();i++)
            for (int j=0; j<glasn.length;j++)
                if (str3.charAt(i)==glasn[j]) g3++;

        System.out.println("в этом слове всего столько гласных = "+g3);

//        4.
//        Посчитайте количество согласных букв в слове

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        byte s4=0;
        char soglasn[] = new char[]{'ц','к','н','г','ш','щ','з','х','ф','в','п','р','л','д','ж','ч','с','м','т','б'};

        for (int i=0;i<str3.length();i++)
            for (int j=0; j<soglasn.length;j++)
                if (str3.charAt(i)==soglasn[j]) s4++;

        System.out.println("в этом слове всего столько согласных = "+s4);
//        5.
//        Выведите слово наоборот, при этом столбиком

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        for (int i=str3.length();i>0;i--) System.out.println(str3.charAt(i-1));
//        6.
//        Замените все гласные в слове на ‘у’

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        for (int i=0;i<str3.length();i++)
            for (int j=0; j<glasn.length;j++)
                if (str3.charAt(i)==glasn[j]) {
                    str3=str3.replace(str3.charAt(i),'у');

                }
        System.out.println("слово после преобразования = "+ str3);
//        7.
//        Пользователь вводит дробное число, выведите его дробную часть


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner7 = new Scanner(System.in);
        System.out.println("Введите дробное число: ");
        String str7 = scanner7.nextLine();
        int dote = 0;
        dote=str7.indexOf('.')+1;
        System.out.println("Дробная часть вот такая :"+str7.substring(dote));


//        8.
//        Пользователь вводит предложение
//                . Замените в каждом слове этого
//        предложения, последн** д** бук** на **

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner8 = new Scanner(System.in);
        System.out.println("Введите предложение: ");
        String str8 = scanner8.nextLine();

//        String str[] = new String[108];

        String strAr8[] = str8.split(" ");

        for (int i=0; i<strAr8.length;i++) {
            for (int j = 0; j < strAr8[i].length()-2; j++) {
                System.out.printf(String.valueOf(strAr8[i].charAt(j)));
            }
            System.out.print("** ");
        }
//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для ввода данных со стороны пользователя
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5
//        критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов


    }
}